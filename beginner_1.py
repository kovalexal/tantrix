import os
import sys

import numpy as np
from matplotlib import pyplot as plt
import cv2

from accessories import *

def main(argc=len(sys.argv), argv=sys.argv):
    if argc == 1:
        exit()
    fpath = argv[1]

    img = cv2.imread(fpath, cv2.IMREAD_COLOR)
    hexagons, hexagons_centers = find_hexagons(img, draw_result=True)

    print('Число фигур: {}'.format(len(hexagons)))

    # FOLDER = 'data'
    #
    # for fname in os.listdir(FOLDER):
    #     # Find filenames starting with Group_
    #     if not fname.startswith('Group_'):
    #         continue
    #     fpath = os.path.join(FOLDER, fname)
    #
    #     # Read image
    #     img = cv2.imread(fpath, cv2.IMREAD_COLOR)
    #
    #     # Find hexagons on image
    #     hexagons, hexagons_centers = find_hexagons(img, draw_result=True)
    #
    #     print(len(hexagons))



if __name__ == '__main__':
    main()