#!/usr/bin/evn python3

import glob
import os
import sys
import argparse

import numpy as np
from matplotlib import pyplot as plt
import cv2

from accessories import *

def main(argc=len(sys.argv), argv=sys.argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--base', help='path to base image', type=str, required=True)
    parser.add_argument('-p', '--process', help='paths to images for processing', nargs='+', type=str, required=True)
    args = parser.parse_args()

    fpath = args.base
    RADIUS = 20

    # Create a hexagon database
    img_base = cv2.imread(fpath, cv2.IMREAD_COLOR)
    hexagons, hexagons_centers = find_hexagons(img_base, draw_result=False)
    # Delete background
    mask = np.zeros(img_base.shape, dtype=np.uint8)
    cv2.drawContours(mask, hexagons, -1, (255, 255, 255), cv2.FILLED)
    img_base = cv2.bitwise_and(img_base, mask)

    hexagon_base = []
    # Iterate over all hexagons, sorted by leftmost point
    for i, (contour, center) in enumerate(sorted(zip(hexagons, hexagons_centers), key=lambda x: tuple(x[0][x[0][:,:,0].argmin()][0])[0])):
        # Cut a hexagon
        x, y, w, h = cv2.boundingRect(contour)
        hexagon = img_base[y:y + h, x:x + w, :]

        # Move contour and center to new image
        contour -= np.array([x, y])
        center -= np.array([x, y])

        # Process current hexagon
        hexagon, contour, center = transform_hexagon(hexagon, contour, center, RADIUS)
        hexagon = extract_yellow_red(hexagon)

        # Write it to base
        hexagon_base.append([hexagon, contour, center])

    # Process input images
    for fpath_regex in args.process:

        for fpath in glob.glob(fpath_regex):

            img = cv2.imread(fpath, cv2.IMREAD_COLOR)
            hexagons, hexagons_centers = find_hexagons(img, draw_result=False)

            # Remove background
            mask = np.zeros(img.shape, dtype=np.uint8)
            cv2.drawContours(mask, hexagons, -1, (255, 255, 255), cv2.FILLED)
            img_nobackground = cv2.bitwise_and(img, mask)

            for contour, center in zip(hexagons, hexagons_centers):
                # Cut a hexagon
                x, y, w, h = cv2.boundingRect(contour)
                hexagon = img_nobackground[y:y + h, x:x + w, :]

                # Move contour and center to new image
                contour -= np.array([x, y])
                center -= np.array([x, y])

                # Calculate matches to histogram database
                matches = []
                for n in range(6):
                    hexagon_tmp, _, _ = transform_hexagon(hexagon, contour, center, RADIUS, n)
                    hexagon_tmp = extract_yellow_red(hexagon_tmp)
                    matches.append([cv2.matchTemplate(hexagon_tmp, x[0], cv2.TM_CCORR_NORMED) for x in hexagon_base])

                matches = np.max(np.array(matches), axis=0)
                match = np.argmax(matches) + 1

                cv2.drawContours(img, [contour + np.array([x, y])], -1, (0, 255, 0), 2)
                cv2.putText(img, '{}'.format(match), tuple(center + np.array([x, y])), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255), 2)
                cv2.drawContours(mask, hexagons, -1, (255, 255, 255), cv2.FILLED)

            cv2.imshow(fpath, img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()


if __name__ == '__main__':
    main()