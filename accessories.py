import math

import numpy as np
from matplotlib import pyplot as plt
import cv2

def angle(p1, p2, p3):
    '''Returns angle between three points in degrees
    '''

    p1, p2, p3 = np.array(p1), np.array(p2), np.array(p3)
    v1 = p1 - p2
    v2 = p3 - p2
    return np.degrees(np.arccos(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))))

def find_hexagons(img, draw_result=False):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Convert image to grayscale
    img_grayscale = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # Apply CLAHE histogram normalization
    clahe = cv2.createCLAHE(clipLimit=0.1, tileGridSize=(40, 40))
    img_grayscale = clahe.apply(img_grayscale)

    # Otsu`s threshold
    filtered = cv2.bilateralFilter(img_grayscale, 9, 75, 75)
    threshold, img_bin = cv2.threshold(filtered, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Find contours and fill all holes inside
    _, contours, _ = cv2.findContours(img_bin.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(img_bin, [cv2.convexHull(x) for x in contours], -1, 255, cv2.FILLED)
    # cv2.drawContours(img, [cv2.convexHull(x) for x in contours], -1, (155, 255, 255), cv2.FILLED)

    # Apply erosion
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    img_bin = cv2.erode(img_bin, kernel, iterations=1)

    # Find hexagons on image
    _, contours, _ = cv2.findContours(img_bin.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    hexagons = []
    hexagons_centers = []
    for cnt in contours:
        # Approximate contour with polygon
        cnt = cv2.approxPolyDP(cnt, cv2.arcLength(cnt, True) * 0.01, True).reshape(-1, 2)

        # Delete polygons with less than 6 points
        if cnt.shape[0] < 6:
            continue

        # Delete points which angle is more than mean angle
        angles = np.array([angle(cnt[i - 1], cnt[i], cnt[(i + 1) % len(cnt)]) for i in range(len(cnt))])
        angle_threshold = np.mean(angles)
        mask = np.logical_and(angle_threshold * 0.8 <= angles, angles <= angle_threshold * 1.2)
        cnt = cnt[mask]

        # Delete polygons with less than 6 points
        if cnt.shape[0] < 6:
            continue

        # Mean points which length to next is less than half of median
        lengths = np.array([np.linalg.norm(cnt[i] - cnt[i - 1]) for i in range(len(cnt))])
        length_threshold = np.median(lengths) / 2
        mask = lengths < length_threshold
        cnt[mask] = ((cnt[mask] + cnt[np.roll(mask, -1)]) / 2).astype(int)
        cnt = cnt[~np.roll(mask, -1)]

        # Delete polygons all number of points but 6
        if cnt.shape[0] != 6:
            continue

        # Convert back to OpenCV array
        cnt = cnt.reshape(-1, 1, 2)

        # Threshold by aspect ratio
        _, _, w, h = cv2.boundingRect(cnt)
        if not(0.7 <= w / h <= 1.3):
            continue

        # Find centroid of hexagon
        M = cv2.moments(cnt)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        centroid = np.array([cx, cy], dtype=int)

        # Write results
        hexagons.append(cnt)
        hexagons_centers.append(centroid)

    hexagons, hexagons_centers = np.array(hexagons), np.array(hexagons_centers)

    # Threshold by area (rule of three std)
    if len(hexagons):
        areas = np.array([cv2.contourArea(x) for x in hexagons])
        mean_area, std_area = np.mean(areas), np.std(areas)
        mask = np.logical_and(mean_area - 3 * std_area <= areas, areas <= mean_area + 3 * std_area)
        hexagons, hexagons_centers = hexagons[mask], hexagons_centers[mask]

    if draw_result == True:
        for h, c in zip(hexagons, hexagons_centers):
            for p in h:
                cv2.circle(img, tuple(*p), 5, (255, 0, 0), thickness=-1)
            cv2.circle(img, tuple(c), 5, (255, 0, 0), thickness=-1)

        # Draw an image (using matplotlib)
        plt.subplot(121)
        plt.title('Input image')
        plt.imshow(img)
        plt.xticks([]), plt.yticks([])

        plt.subplot(122)
        plt.title('Processed image')
        plt.imshow(img_bin, cmap='gray')
        plt.xticks([]), plt.yticks([])

        mng = plt.get_current_fig_manager()
        mng.full_screen_toggle()
        plt.show()

    return hexagons, hexagons_centers

def transform_hexagon(hexagon, contour, center, radius, n=0):
    contour = np.roll(contour, n, axis=0)

    pts1 = contour.reshape(-1, 2).astype(np.float32).astype(int)
    shape, center, pts2 = generate_hexagon(radius)
    M, retval = cv2.findHomography(pts1, pts2)
    hexagon = cv2.warpPerspective(hexagon, M, shape)

    return hexagon, pts2.astype(int), center

def extract_yellow_red(hexagon):
    hexagon = cv2.bilateralFilter(hexagon, 30, 10, 10)

    hsv = cv2.cvtColor(hexagon, cv2.COLOR_BGR2HSV)
    new_hsv = np.zeros(hsv.shape, dtype=np.uint8)

    yellow = cv2.inRange(hsv, (15, 127, 178), (35, 255, 255))
    new_hsv[yellow.astype(bool)] = [30, 255, 255]

    red = cv2.inRange(hsv, (0, 127, 140), (14, 255, 255))
    new_hsv[red.astype(bool)] = [0, 255, 255]

    new_hexagon = cv2.cvtColor(new_hsv, cv2.COLOR_HSV2BGR)

    return new_hexagon

def generate_hexagon(radius):
    sin60 = np.sin(np.deg2rad(60))
    cos60 = np.cos(np.deg2rad(60))

    dx = sin60 * radius
    dy = cos60 * radius

    shape = (int(2 * dx), int(2 * radius))

    center = (np.array(shape) / 2).ravel()


    points = [
        center + np.array([0, -radius]),
        center + np.array([-dx, -dy]),
        center + np.array([-dx, dy]),

        center + np.array([0, radius - 1]),
        center + np.array([dx - 1, dy]),
        center + np.array([dx - 1, -dy]),
    ]

    return shape, center, np.array(points)
